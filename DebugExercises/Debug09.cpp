#include <iostream>

int main()
{
  int shape;
  double degree , transition;
  shape = 17;
  transition = 100.0 / shape;
  degree = 0;
  for ( ; ; ) {
    if ( degree > 100.0 ) {
      std::cout << degree << " is greater than 100.0\n";
      break;
    }
    else if ( degree < 100.0 )
      std::cout << degree << " is smaller than 100.0\n";
    else
      std::cout << degree << " is equal to 100.0\n";
    degree += transition;
  }
  return 0;
}