#include <stdio.h>

int no_of_trees()
{
  /* compute, compute */ return 105000;
}

int leaves_per_tree()
{
  /* compute some more */ return 25000;
}

long total()
{
  return no_of_trees() * leaves_per_tree();
}

int main()
{
  printf("there are %ld leaves in the forest\n",
    total());
}