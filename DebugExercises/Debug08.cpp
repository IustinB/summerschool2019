#include <string.h>
#include <iostream>

#define Whiskey          1
#define Vodka            2
#define DryVermouth      4
#define IrishCream       8
#define CrDeCacoa     0x10
#define CrDeMenthe    0x20
#define Grenadine     0x40
#define LimeJuice     0x80

struct Recipe
{
  unsigned id;
  const char *name;
  unsigned ingred;
} drinks[] = {
    1 , "Wild Irish Rose" , Whiskey | Grenadine | LimeJuice ,
    2 , "Irish Martini" , Whiskey | DryVermouth | Vodka ,
    3 , "Irish Paddy" , CrDeCacoa | CrDeMenthe | IrishCream ,
    0 , "" , 0
  };

unsigned find_recipe_ingred( const char *name )
{
  for ( unsigned i = 0;
        drinks[ sizeof( drinks ) / sizeof( drinks[ 0 ] ) ].id;
        i++ )
    if ( strcmp( drinks[ i ].name , name ) == 0 )
      return drinks[ i ].ingred;
  return 0;
}

int main()
{
  const auto recipe_name = "Irish Patty";
  const auto recipe_ingred = find_recipe_ingred( recipe_name );
  std::cout << "Ingredients for '" << recipe_name << "' have" << ( recipe_ingred == 0 ? " not" : "" ) << " been found" << std::endl;
  return 0;
}