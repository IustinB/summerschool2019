#include <iostream>

int count_vowels( char *s )
{
  int sum = 0;
  for ( ;; )
    switch ( *s++ ) {
    case 'a':
    case 'e':
    case 'i':
    case 'o':
    case 'u':
      sum++;
      continue;
    default:
      continue;
    case '\0':
      break;
  }
  return sum;
}

void main( void )
{
  const auto vowelCount = count_vowels( "this text contains some vowels" );
  std::cout << vowelCount << std::endl;
}