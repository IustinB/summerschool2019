#include <iostream>

bool isPrime( int n )
{
  if ( n % 2 == 0 ) return false;
  int m = 3;
  while ( m*m < n ) {
    if ( n%m == 0 ) return false;
    m += 2;
  }
  return true;
}

int main()
{
  int input;
  std::cout << "Please enter an integer: ";
  std::cin >> input;
  if ( isPrime( input ) )
    std::cout << input << " is a prime number.\n";
  else
    std::cout << input << " is not a prime number.\n";
  return 0;
}